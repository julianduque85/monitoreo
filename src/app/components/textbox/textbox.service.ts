export class textBoxProperties {
    index:number = 0;
    placeholder:string = '';
    label:string = '';
    value:string = '';
    clearInput:boolean = false;
    typebox:string = 'text';
    counter:boolean = false;
    maxlength:number = 150;
    ngModelName:string = '';
}