import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { textBoxProperties } from './textbox.service';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss'],
})
export class TextboxComponent  implements OnInit {

  @Input() property:textBoxProperties;
  @Output() eventChange = new EventEmitter<textBoxProperties>();
  constructor() { }

  ngOnInit() {}
  onChangeText(){
    this.eventChange.emit(this.property);
  }

}
