import { Component, OnInit } from '@angular/core';
import { textBoxProperties } from '../components/textbox/textbox.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent  implements OnInit {


  user:textBoxProperties;
  password:textBoxProperties;

  constructor() { 


    this.user.index = 1;
    this.user.label = 'Usuario';
    this.user.placeholder = 'correo@correo.com';
    this.user.ngModelName = 'nick';
    this.user.clearInput = true;

    this.password.index = 2;
    this.password.label = 'Clave';
    this.password.placeholder = '*****';
    this.password.ngModelName = 'password';
    this.password.clearInput = true;
    
  }

  ngOnInit() {}

  eventChangeText(event:any)
  {
    debugger
  }

}
